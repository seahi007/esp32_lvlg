/**
 * @file gps_massge.h
 * @author your name (you@domain.com)
 * @brief  GPS 信息解析头文件
 * @version 0.1
 * @date 2021-12-10
 *
 * @copyright Copyright (c) 2021
 *
 */


#ifndef GPS_MASSGE_H
#define GPS_MASSGE_H

extern QueueHandle_t gps_massge_queue;

typedef struct {
    int     smask;      /**<指定从中获取数据的包类型的掩码 */
    int     sig;        /**< GPS质量指示器（0=无效；1=固定；2=差分，3=敏感） */
    int     fix;        /**< 操作模式，用于导航（1=固定不可用；2=2D；3=3D） */

    double  PDOP;       /**< 位置精度稀释 */
    double  HDOP;       /**< 精度水平稀释*/
    double  VDOP;       /**< 精度的垂直稀释 */

    double  lat;        /**< 以NDEG-+/-[度][min]为单位的纬度。[sec/60] */
    double  lon;        /**< 经度单位为NDEG-+/-[度][min]。[sec/60] */
    double  elv;        /**< 天线高度高于/低于平均海平面（大地水准面），单位为米*/
    double  speed;      /**< 地面速度（公里/小时） */
    double  direction;  /**< 跟踪角度（以度为单位）*/
    double  declination; /**< 磁变化度（从实际航向减去东风变化）*/
}gps_msg_t;

extern gps_msg_t GPS_MSG; //GPS 信息结构体

void gps_massge_queue_init();

#endif
