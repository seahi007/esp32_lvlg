#ifndef _MY_GUI_H
#define _MY_GUI_H

#include "esp_freertos_hooks.h"

#include "esp_system.h"
#include "driver/gpio.h"
#include "../../lvgl/lvgl.h"
#include "../../lvgl_esp32_drivers/lvgl_helpers.h"

lv_obj_t* SN_label;

void app_lvgl_display_task(void* arg);
#endif



