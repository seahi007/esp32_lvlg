#ifndef GPS_LOCATION_H
#define GPS_LOCATION_H

#include "my_gui.h"

#include "esp_system.h"
#include "driver/gpio.h"
#include "driver/uart.h"
#include "esp_err.h"
#define UART_PIN_TX (17)
#define UART_PIN_RX (16)
#define UART_PIN_NO_CHANGE (-1)

#define BUF_SIZE (1024)
#define RD_BUF_SIZE (BUF_SIZE)
#define GATT_ENABLE 1
#define GATT_DISENABLE 0

/**
 * @brief 创建指令结构体 str 一般填 "OK"
 *
 */
typedef struct {

    esp_err_t(*EC_01G_RST)(void);
    char* (*EC_01G_CGSN)(void);
    esp_err_t(*EC_01G_CGATT)(bool enable);
    esp_err_t(*EC_01G_CGDCONT)(void);
    esp_err_t(*EC_01G_CGACT)(void);
    esp_err_t(*EC_01G_CREG)(void);
    esp_err_t(*EC_01G_GPS)(int out_time_s);
    esp_err_t(*EC_01G_NO_GPS)(void);
}EC_01G_t;
/**
 * @brief 指令状态枚举
 *
 */
typedef enum {
    AT_DEF = 0,
    AT_ERROR,
    AT_OK,
    AT_GET_SN_OK,

}ec_at_type_t;

/**
 * @brief 变量声明
 *
 */
extern char* SIM_SN; //SN 号
extern ec_at_type_t EC_AT_FLAG; //AT的状态
extern int GPS_MSG_OUT_TIME; //GPS信息 输出时间
extern EC_01G_t ec_01g_setup; //GPS 操作结构体
/**
 * @brief 函数声明
 *
 */
void EC_01G_Init(void);

#endif
