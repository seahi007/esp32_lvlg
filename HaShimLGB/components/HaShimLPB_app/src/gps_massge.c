#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_err.h"
#include "esp_freertos_hooks.h"
#include "gps_massge.h"
#include "gps_location.h"
#include "esp_log.h"
static char* TAG = "GPS MASSGE";

QueueHandle_t gps_massge_queue;
gps_msg_t GPS_MSG;
static void gps_massge_handler(void* arg);
/**
 * @brief 创建卫星信息消息队列
 *
 * @return esp_err_t
 */
void gps_massge_queue_init()
{
    gps_massge_queue = xQueueCreate(5, 256);

    xTaskCreate(gps_massge_handler, "gps massge handler task", 1024 * 2, NULL, 10, NULL);

}
/**
 * @brief GPS 信息解析
 *
 * @param arg
 */
static void gps_massge_handler(void* arg)
{
    //$GPRMC,024813.640,A,3158.4608,N,11848.3737,E,10.05,324.27,150706,,,A*50
    for (;; ) {
        char* gps_msg = malloc(256);
        xQueueReceive(gps_massge_queue, gps_msg, portMAX_DELAY);
        ESP_LOGW(TAG, "%s", gps_msg);
        gps_msg += 7;
        //使用 strtok 解析信息

        free(gps_msg);
    }

    vTaskDelete(NULL);
}
