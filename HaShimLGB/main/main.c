#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_freertos_hooks.h"
#include "freertos/semphr.h"
#include "esp_system.h"
#include "driver/gpio.h"
#include "my_gui.h"
#include "gps_location.h"

void app_main(void)
{

    xTaskCreate(app_lvgl_display_task, "lvgl display task", 1024 * 3, NULL, 10, NULL);
    EC_01G_Init();
    while (1) {
        vTaskDelay(pdMS_TO_TICKS(100));
    }
}
